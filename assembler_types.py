from enum import Enum

letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
hexdigits = "0123456789ABCDEF"
numbers = "0123456789"

BYTE_HI = 0
BYTE_LO = 1


class AddressingMode(Enum):
    Illegal = 0
    Immediate8 = 1
    Immediate16 = 2
    Absolute8 = 3
    Absolute16 = 4
    IndirectIndexed = 5
    IndexedIndirect = 6
    Relative = 7
    Implied = 8
    Absolute16OffsetX = 9
    Absolute16OffsetY = 10
    Symbol = 11
    Indirect = 12
    # Not really addressing modes
    StringLiteral = 13
    ByteLiteral = 14
    WordLiteral = 15


class SymbolReplacement:
    def __init__(self, symbol, addressing_mode, isolate_byte=None):
        self.symbol = symbol
        self.addressing_mode = addressing_mode
        self.isolate_byte = isolate_byte


class AssemblyState:
    def __init__(self):
        self.pc = 0
        self.initial_pc = 0
        self.symbols = {}
        self.replace_with_symbol_locations = {}
        self.output_bytes = bytearray(65536)
        self.previous_global_label = None


###############################
class ExpressionTokenKind(Enum):
    LITERALSIGN = 0
    SYMBOL = 1
    HEXSIGN = 2
    LOBYTE = 3
    HIBYTE = 4
    NUMBER = 5


class ExpressionToken:
    def __init__(self, kind, value):
        self.kind = kind
        self.value = value


def evaluate_expression(value: str):
    # print(value)
    contains_symbol = False

    expression_tokens = []

    value = value.upper()

    char_pointer = 0

    while char_pointer < len(value):
        c = value[char_pointer]

        if c == ">":
            expression_tokens.append(
                ExpressionToken(ExpressionTokenKind.HIBYTE, None)
            )
            char_pointer += 1

        elif c == "<":
            expression_tokens.append(
                ExpressionToken(ExpressionTokenKind.LOBYTE, None)
            )
            char_pointer += 1

        elif c == "$":
            expression_tokens.append(
                ExpressionToken(ExpressionTokenKind.HEXSIGN, None)
            )
            char_pointer += 1

        elif c == "#":
            expression_tokens.append(
                ExpressionToken(ExpressionTokenKind.LITERALSIGN, None)
            )
            char_pointer += 1

        else:
            # Is this a number or a symbol?
            number_starts = char_pointer
            number_ends = char_pointer

            is_number = True
            while number_ends < len(value):
                if value[number_ends] not in hexdigits:
                    is_number = False
                number_ends += 1

            if is_number:
                is_hex = False
                for token in expression_tokens:
                    if token.kind == ExpressionTokenKind.HEXSIGN:
                        is_hex = True

                if is_hex:
                    expression_tokens.append(
                        ExpressionToken(ExpressionTokenKind.NUMBER, int(value[number_starts:number_ends], 16))
                    )
                else:
                    expression_tokens.append(
                        ExpressionToken(ExpressionTokenKind.NUMBER, int(value[number_starts:number_ends]))
                    )

            else:
                expression_tokens.append(
                    ExpressionToken(ExpressionTokenKind.SYMBOL, value[number_starts:number_ends])
                )
            char_pointer = number_ends


#       elif c == "#":
#           starts_at = 0
#
#           if value[char_pointer + 1] != "$":
#                starts_at = char_pointer + 1
#            else:
#                starts_at = char_pointer + 2
#
#            ptr = starts_at
#
#            while ptr < len(value) and value[ptr] in hexdigits:
#                ptr += 1
#            length = ptr - starts_at
#
#            expression_tokens.append(
#                ExpressionToken(ExpressionTokenKind.LITERAL, int(value[1:].replace("$", "0x"), 16))
#            )
#            char_pointer = starts_at + length
#
#       else:
#          char_pointer = char_pointer + 1

    result = 0

    need_hibyte = False
    need_lobyte = False
    is_hex = False

    for token in expression_tokens:
        if token.kind == ExpressionTokenKind.HIBYTE:
            need_hibyte = True
        elif token.kind == ExpressionTokenKind.LOBYTE:
            need_lobyte = True

        result = token.value

    try:
        int(result)
    except ValueError:
        return {"result": result, "kind": str, "only_hibyte": need_hibyte, "only_lobyte": need_lobyte}

    if need_hibyte:
        return (result & 0xFF00) >> 8
    elif need_lobyte:
        return result & 0xFF

    return result

###############################
class Operand:
    def __init__(self, value: str, state, expected_mode=AddressingMode.Absolute16, is_statement=False):
        self.addressing_mode = AddressingMode.Illegal
        self.data = 0
        self.bytes = 0
        self.isolate_byte = None

        # Determine the addressing mode.
        if value is None:
            self.addressing_mode = AddressingMode.Implied
            return

        value = value.strip()

        # This is always going to be 8-bit immediate.
        if value[0] == "#":
            self.addressing_mode = AddressingMode.Immediate8

            # Is this an ASCII byte or an integer?
            if value[1] == "'" and value[3] == "'":
                # Interpret as an ASCII byte.
                self.data = ord(value[2])
                self.bytes = 1
            else:
                self.data = evaluate_expression(value)
                self.bytes = 1

        # This could be Abs16, Abs16X, or Abs16Y.
        elif value[0] == "$":
            self.bytes = 2

            if value[-2:] == ",X":
                self.addressing_mode = AddressingMode.Absolute16OffsetX
                self.data = evaluate_expression(value[:-2])
            elif value[-2:] == ",Y":
                self.addressing_mode = AddressingMode.Absolute16OffsetY
                self.data = evaluate_expression(value[:-2])
            else:
                self.addressing_mode = AddressingMode.Absolute16
                self.data = evaluate_expression(value)

        # This could be ($NNNN,X), ($NNNN),Y, or ($NNNN).
        elif value[0] == "(":
            self.bytes = 2

            if value[-2].upper() == "X":
                self.addressing_mode = AddressingMode.IndexedIndirect
                self.data = evaluate_expression(value[1:-3])
            elif value[-1].upper() == "Y":
                self.addressing_mode = AddressingMode.IndirectIndexed
                self.data = evaluate_expression(value[1:-3])
            else:
                self.addressing_mode = AddressingMode.Indirect
                self.data = evaluate_expression(value[1:-1])

        elif value[0] in numbers:
            # It's a raw number or it's an error.
            self.data = int(value)

            if self.data < 255:
                self.addressing_mode = AddressingMode.ByteLiteral
            else:
                self.addressing_mode = AddressingMode.WordLiteral

        elif value.startswith("\"") and value.endswith("\""):
            # It's a quoted string.
            self.data = value[1:-1]
            self.addressing_mode = AddressingMode.StringLiteral

        else:
            # Uh... maybe it's a symbol?
            state.replace_with_symbol_locations[state.pc + 1] = SymbolReplacement(value, expected_mode)

            if value[-2:] == ",X":
                self.addressing_mode = AddressingMode.Absolute16OffsetX
                self.data = 0
            elif value[-2:] == ",Y":
                self.addressing_mode = AddressingMode.Absolute16OffsetY
                self.data = 0
            else:
                self.addressing_mode = expected_mode
                self.data = 0

        if type(self.data) is dict:
            # We got a symbol back.
            state.replace_with_symbol_locations[state.pc + 1] = SymbolReplacement(self.data, self.addressing_mode)
            self.data = 0 # Dummy the result out now that we've saved it.


class Error:
    ERR_INVALID_ADDRESSING_MODE = "Invalid addressing mode"
