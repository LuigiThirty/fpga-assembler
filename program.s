    .org $F000

    ; .equ ACC16LO $0002
    ; .equ ACC16HI $0003

    .equ UART_IN $A000

Entry:
    ; TODO: hold CPU in reset for one second on power-up
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP

    ; set up stack and registers
    LDA #$FF
    TAS

BranchTest:
    LDX #120
    LDY #$00
    STX $8000
    STY $8001

    LDA #<STR_BranchTest
    STA $0000
    LDA #>STR_BranchTest
    STA $0001
    JSR PrintString

    LDA #$20
    STA $8002

    ; Tests:    LDA imm8, LDX imm8, LDY imm8
    ;           LDA Abs16 LDX Abs16 LDY Abs16
    ;           STA Abs16 STX Abs16 STY Abs16
    ;           CMP imm8, CPX imm8, CPY imm8
    ;           BRA, BNE, BEQ

    LDA #$10
    CMP #$10
    BNE BT1_Fail

    LDA #$20
    CMP $0010
    BEQ BT1_Fail

    LDX #$11
    CPX #$11
    BNE BT1_Fail

    LDX #$11
    CPX #$22
    BEQ BT1_Fail

    LDY #$12
    CPY #$12
    BNE BT1_Fail

    LDY #$12
    CPY #$24
    BEQ BT1_Fail

    BRA OK1

BT1_Fail:
    JMP BT_Fail

OK1:
    LDA #<STR_OK1
    STA $0000
    LDA #>STR_OK1
    STA $0001
    JSR PrintString

    LDX #$F0
    STX $0000

    ; Test LDX
    LDA #$22
    STA $0012

    LDX $0012
    CPX #$22
    BNE BT_Fail

    LDX $0012
    CPX #$44
    BEQ BT_Fail

OK2:
    LDA #<STR_OK2
    STA $0000
    LDA #>STR_OK2
    STA $0001
    JSR PrintString

    ; Test LDY
    LDY #$40
    STY $0013

    LDY $0013
    CPY #$40
    BNE BT_Fail

    LDY $0013
    CPY #$30
    BEQ BT_Fail

OK3:
    LDA #<STR_OK3
    STA $0000
    LDA #>STR_OK3
    STA $0001
    JSR PrintString
    BRA BT_Pass

BT_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

BT_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    BRA SR_Test

    ;;;;;;;;;;
SRTestRoutine:
    LDY #$02
    STY $9001

    LDA #$90

    RTS

    JMP SR_Fail

SR_Test:
    LDX #60
    LDY #0
    JSR MoveCursor

    LDX #160
    LDY #$00
    STX $8000
    STY $8001

    LDA #<STR_JSRTest
    STA $0000
    LDA #>STR_JSRTest
    STA $0001
    JSR PrintString

    LDA #$20
    STA $8002

    ;;;
    LDA #$80
    JSR SRTestRoutine

    CMP #$90
    BNE SR_Fail

SR_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    JMP SHIFT_Test

SR_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;;;;;;;;;;;;;;;;;;
SHIFT_Test:
    LDX #200
    LDY #0
    JSR MoveCursor

    LDA #<STR_ShiftTest
    STA $0000
    LDA #>STR_ShiftTest
    STA $0001
    JSR PrintString

    LDY #0

    ; Test immediate mode.
    LDA #$10
    ASL

    CMP #$20
    BNE SHIFT_ASLFail

    INY

    LSR
    CMP #$10
    BNE SHIFT_LSRFail

    INY

    LDA #$FF
    ASL
    BCC SHIFT_ASLFail

    INY

    LDA #$FF
    LSR
    BCC SHIFT_LSRFail

    INY

    ; Test Abs16 mode.
    LDA #$10
    STA $0000

    ASL $0000
    LDA $0000
    CMP #$20
    BNE SHIFT_ASLFail

    INY

    LSR $0000
    LDA $0000
    CMP #$10
    BNE SHIFT_LSRFail

    INY

    LDA #$FF
    STA $0000
    ASL $0000
    BCC SHIFT_ASLFail

    INY

    LDA #$FF
    STA $0000
    LSR $0000
    BCC SHIFT_LSRFail

    INY

SHIFT_Pass:


    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    JMP INC_Test

SHIFT_ASLFail:
    STA $9001
    STY $9000
    BRK

    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    LDA #<STR_ASL
    STA $0000
    LDA #>STR_ASL
    STA $0001
    JSR PrintString

    JMP Done

SHIFT_LSRFail:
    STA $9001
    STY $9000
    BRK

    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    LDA #<STR_LSR
    STA $0000
    LDA #>STR_LSR
    STA $0001
    JSR PrintString

    JMP Done

;;;;;;;;;;;;;;;;;;
INC_Test:
    LDX #240
    LDY #0
    JSR MoveCursor

    LDA #<STR_INCTest
    STA $0000
    LDA #>STR_INCTest
    STA $0001
    JSR PrintString

    LDA #$FF
    INA
    CMP #0
    BNE INC_Fail

    LDX #$FF
    INX
    CPX #0
    BNE INC_Fail

    LDY #$FF
    INY
    CPY #0
    BNE INC_Fail

    LDA #$FF
    STA $0000
    INC $0000
    LDA $0000
    CMP #0
    BNE INC_Fail

INC_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    JMP DEC_Test

INC_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

DEC_Test:
    LDX #24
    LDY #1
    JSR MoveCursor

    LDA #<STR_DECTest
    STA $0000
    LDA #>STR_DECTest
    STA $0001
    JSR PrintString

    LDA #$FF
    DEA
    CMP #$FE
    BNE DEC_Fail

    LDX #$FF
    DEX
    CPX #$FE
    BNE DEC_Fail

    LDY #$FF
    DEY
    CPY #$FE
    BNE DEC_Fail

    LDA #$FF
    STA $0000
    DEC $0000
    LDA $0000
    CMP #0
    BNE DEC_Fail

DEC_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    JMP OR_Test

DEC_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;;
OR_Test:
    LDX #64
    LDY #1
    JSR MoveCursor

    LDA #<STR_ORTest
    STA $0000
    LDA #>STR_ORTest
    STA $0001
    JSR PrintString

    ; Prepare for absolute address tests
    LDA #$10
    STA $0000
    LDA #$20
    STA $0001

    LDA #$01
    OR  #$10
    CMP #$11
    BNE OR_Fail

    LDA #$02
    OR  $0000
    CMP #$12
    BNE OR_Fail

    LDA #$03
    LDX #1
    OR  $0000,X
    CMP #$23
    BNE OR_Fail

OR_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    JMP AND_Test

OR_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;;
AND_Test:
    LDX #104
    LDY #1
    JSR MoveCursor

    LDA #<STR_ANDTest
    STA $0000
    LDA #>STR_ANDTest
    STA $0001
    JSR PrintString

    ; Prepare for absolute address tests
    LDA #$10
    STA $0000
    LDA #$20
    STA $0001

    LDA #$FF
    AND #$10
    CMP #$10
    BNE AND_Fail

    LDA #$FF
    AND $0000
    CMP #$10
    BNE AND_Fail

    LDX #1
    LDA #$FF
    AND $0000,X
    CMP #$20
    BNE AND_Fail

AND_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    BRA XOR_Test

AND_Fail:

    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;;
;;
XOR_Test:
    LDX #144
    LDY #1
    JSR MoveCursor

    LDA #<STR_XORTest
    STA $0000
    LDA #>STR_XORTest
    STA $0001
    JSR PrintString

    ; Prepare for absolute address tests
    LDA #$10
    STA $0000
    LDA #$20
    STA $0001

    LDA #$FF
    XOR #$10
    CMP #$EF
    BNE XOR_Fail

    LDA #$FF
    XOR $0000
    CMP #$EF
    BNE XOR_Fail

    LDX #1
    LDA #$FF
    XOR $0000,X
    CMP #$DF
    BNE XOR_Fail

XOR_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    BRA ROL_Test

XOR_Fail:

    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;;
ROL_Test:
    LDX #184
    LDY #1
    JSR MoveCursor

    LDA #<STR_ROLTest
    STA $0000
    LDA #>STR_ROLTest
    STA $0001
    JSR PrintString

    ; Prepare for absolute address tests
    LDA #$80
    STA $0000
    STA $0001

    CLC
    LDA #$80
    ROL
    BCC ROL_Fail
    ROL
    CMP #$01
    BNE ROL_Fail

    CLC
    LDA #$80
    STA $0000
    ROL $0000
    BCC ROL_Fail
    ROL $0000
    LDA $0000
    CMP #$01
    BNE ROL_Fail

    CLC
    LDA #$80
    LDX #1
    STA $0000,X
    ROL $0000,X
    BCC ROL_Fail
    ROL $0000,X
    LDA $0000,X
    CMP #$01
    BNE ROL_Fail

ROL_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    BRA ROR_Test

ROL_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;;
ROR_Test:
    LDX #224
    LDY #1
    JSR MoveCursor

    LDA #<STR_RORTest
    STA $0000
    LDA #>STR_RORTest
    STA $0001
    JSR PrintString

    ; Prepare for absolute address tests
    LDA #$80
    STA $0000
    STA $0001

    CLC
    LDA #$01
    ROR
    BCC ROR_Fail
    ROR
    CMP #$80
    BNE ROR_Fail

    CLC
    LDA #$01
    STA $0000
    ROR $0000
    BCC ROR_Fail
    ROR $0000
    LDA $0000
    CMP #$80
    BNE ROR_Fail

    CLC
    LDA #$01
    LDX #1
    STA $0000,X
    ROR $0000,X
    BCC ROR_Fail
    ROR $0000,X
    LDA $0000,X
    CMP #$80
    BNE ROR_Fail

ROR_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    BRA ADC_Test

ROR_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;;
ADC_Test:
    LDX #8
    LDY #2
    JSR MoveCursor

    LDA #<STR_ADCTest
    STA $0000
    LDA #>STR_ADCTest
    STA $0001
    JSR PrintString

    LDA #$01
    STA $0002

    CLC
    LDA #$80
    ADC #$10
    CMP #$90
    BNE ADC_Fail

    SEC
    ADC #1
    CMP #$92
    BNE ADC_Fail

    ;
    LDA #$40
    STA $0000
    CLC
    LDA #$80
    ADC $0000
    CMP #$C0
    BNE ADC_Fail
    SEC
    ADC $0002
    CMP #$C2
    BNE ADC_Fail

ADC_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    BRA SBC_Test

ADC_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;;
SBC_Test:
    LDX #48
    LDY #2
    JSR MoveCursor

    LDA #<STR_SBCTest
    STA $0000
    LDA #>STR_SBCTest
    STA $0001
    JSR PrintString

    CLC
    LDA #$80
    SBC #$10
    CMP #$6F
    SBC #$1
    CMP #$6E
    BNE SBC_Fail

    SEC
    LDA #$20
    STA $0000
    LDA #$80
    SBC $0000
    SBC #$1
    CMP #$5F
    BNE SBC_Fail

SBC_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    JMP STACK_Test

SBC_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done

;; Stack test
STACK_Test:
    LDX #88
    LDY #2
    JSR MoveCursor

    LDA #<STR_STACKTest
    STA $0000
    LDA #>STR_STACKTest
    STA $0001
    JSR PrintString

    LDA #1
    LDX #2
    LDY #3

    PHA
    PHX
    PHY

    PLX
    PLA
    PLY

    CPX #3
    BNE STACK_Fail
    CMP #2
    BNE STACK_Fail
    CPY #1
    BNE STACK_Fail

STACK_Pass:
    LDA #<STR_Pass
    STA $0000
    LDA #>STR_Pass
    STA $0001
    JSR PrintString

    JMP Done

STACK_Fail:
    LDA #<STR_Fail
    STA $0000
    LDA #>STR_Fail
    STA $0001
    JSR PrintString

    JMP Done


;;;;;;;;;;;;;;;;;;
STR_Banner:
    .asciiz "CANIS 8 BOOT ROM"

    ;;; Test names
STR_BranchTest:
    .asciiz "BRANCH "

STR_JSRTest:
    .asciiz "JSR "

STR_ShiftTest:
    .asciiz "SHIFT "

STR_INCTest:
    .asciiz "INC "

STR_DECTest:
    .asciiz "DEC "

STR_ORTest:
    .asciiz "OR "

STR_ANDTest:
    .asciiz "AND "

STR_XORTest:
    .asciiz "XOR "

STR_ROLTest:
    .asciiz "ROL "

STR_RORTest:
    .asciiz "ROR "

STR_ADCTest:
    .asciiz "ADC "

STR_SBCTest:
    .asciiz "SBC "

STR_STACKTest:
    .asciiz "STACK "

STR_Count:
    .asciiz "0123456789012345678901234567890123456789"

STR_OK1:
    .asciiz "OK1 "

STR_OK2:
    .asciiz "OK2 "

STR_OK3:
    .asciiz "OK3 "

STR_Pass:
    .asciiz "PASS "

STR_Fail:
    .asciiz "FAIL "

STR_ASL:
    .asciiz "ASL "

STR_LSR:
    .asciiz "LSR "

;;;;;;;;;;;;;
MoveCursor:
    ; X:Y -> 16-bit tile address, little endian
    STX $8000
    STY $8001
    RTS

MoveCursorDebug:
    ; X:Y -> 16-bit tile address, little endian

    ; One row is 40 tiles wide.
    ; To convert (col,row) to address:
    ;   address = (row * 40) + col

    ; But we're using a lookup table.
    ; $02 = lo $03 = hi
    TYA
    ASL
    TAY

    LDA TileYLookup,Y
    STA $0002
    INY
    LDA TileYLookup,Y
    STA $0003

    TXA
    BCC MoveCursorDebug_Write

    INC $0003

MoveCursorDebug_Write:
    ;STA $0002

    LDA $0002
    STA $8000
    LDA $0003
    STA $8001

    RTS

PrintString:
    LDY #0

    LDA $0000
    STA $9001
    LDA $0001
    STA $9000

PrintString_Loop:
    ; The string pointer is at $00.
    LDA ($0000),Y
    CMP #0
    BEQ PrintString_Done

    STA $8002
    INY
    BRA PrintString_Loop

PrintString_Done:
    RTS

Done:
    LDA #$AA
    STA $9000
    BRK

Test:
    LDX #$22
    RTS

    ; Lookup table that converts tile Y to memory offset.
TileYLookup:
    dw 0
    dw 40
    dw 80
    dw 120
    dw 160
    dw 200
    dw 240
    dw 280
    dw 320
    dw 360
    dw 400
    dw 440
    dw 480
    dw 520
    dw 560
    dw 600
    dw 640
    dw 680
    dw 720
    dw 760
    dw 800
    dw 840
    dw 880
    dw 920
    dw 960