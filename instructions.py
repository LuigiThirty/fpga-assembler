import assembler_types
from assembler_types import AddressingMode, Operand, Error



def inst_NOP(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x04
    state.pc += 1


def inst_LDX(state: assembler_types.AssemblyState, value: str):

    opcode = 0
    operand = Operand(value, state)

    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x28
        state.output_bytes[state.pc + 1] = operand.data
        state.pc += 2

    elif operand.addressing_mode == AddressingMode.Absolute16 or operand.addressing_mode == AddressingMode.Absolute8:
        state.output_bytes[state.pc] = 0x29
        state.output_bytes[state.pc + 1] = operand.data & 0x00FF
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3

    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_LDA(state: assembler_types.AssemblyState, value: str):

    operand = Operand(value, state)

    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x20
        state.output_bytes[state.pc + 1] = operand.data
        state.pc += 2

    elif operand.addressing_mode == AddressingMode.Absolute16 or operand.addressing_mode == AddressingMode.Absolute8:
        state.output_bytes[state.pc] = 0x21
        state.output_bytes[state.pc + 1] = operand.data & 0x00FF
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3

    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        state.output_bytes[state.pc] = 0x22
        state.output_bytes[state.pc + 1] = operand.data & 0x00FF
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3

    elif operand.addressing_mode == AddressingMode.Absolute16OffsetY:
        state.output_bytes[state.pc] = 0x23
        state.output_bytes[state.pc + 1] = operand.data & 0x00FF
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3

    elif operand.addressing_mode == AddressingMode.IndirectIndexed:
        state.output_bytes[state.pc] = 0x25
        state.output_bytes[state.pc + 1] = operand.data & 0x00FF
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3

    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_LDY(state: assembler_types.AssemblyState, value: str):

    opcode = 0
    operand = Operand(value, state)

    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x38
        state.output_bytes[state.pc + 1] = operand.data
        state.pc += 2

    elif operand.addressing_mode == AddressingMode.Absolute16 or operand.addressing_mode == AddressingMode.Absolute8:
        state.output_bytes[state.pc] = 0x39
        state.output_bytes[state.pc + 1] = operand.data & 0x00FF
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3

    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_STA(state: assembler_types.AssemblyState, value: str):
    opcode = 0
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Absolute16 or operand.addressing_mode == AddressingMode.Absolute8:
        opcode = 0x41
    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        opcode = 0x42
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = opcode
    state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
    state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
    state.pc += 3


def inst_STX(state: assembler_types.AssemblyState, value: str):

    opcode = 0
    operand = Operand(value, state)

    # Determine the addressing mode. Fold Abs16 and Abs8 together.
    if operand.addressing_mode == AddressingMode.Absolute16 or operand.addressing_mode == AddressingMode.Absolute8:
        opcode = 0x49
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = opcode
    state.output_bytes[state.pc+1] = (operand.data & 0x00FF)
    state.output_bytes[state.pc+2] = (operand.data & 0xFF00) >> 8
    state.pc += 3


def inst_STY(state: assembler_types.AssemblyState, value: str):

    opcode = 0
    operand = Operand(value, state)

    # Determine the addressing mode. Fold Abs16 and Abs8 together.
    if operand.addressing_mode == AddressingMode.Absolute16 or operand.addressing_mode == AddressingMode.Absolute8:
        opcode = 0x59
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = opcode
    state.output_bytes[state.pc+1] = (operand.data & 0x00FF)
    state.output_bytes[state.pc+2] = (operand.data & 0xFF00) >> 8
    state.pc += 3


def inst_BRK(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x00
    state.pc += 1


def inst_INC(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Absolute16:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x31
    state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
    state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
    state.pc += 3


def inst_INA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x01
    state.pc += 1


def inst_INX(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x02
    state.pc += 1


def inst_INY(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x03
    state.pc += 1


def inst_DEC(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Absolute16:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x31
    state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
    state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
    state.pc += 3


def inst_DEA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x11
    state.pc += 1


def inst_DEX(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x12
    state.pc += 1


def inst_DEY(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x13
    state.pc += 1


def inst_JMP(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Absolute16)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Absolute16:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xE1
    state.output_bytes[state.pc+1] = (operand.data & 0x00FF)
    state.output_bytes[state.pc+2] = (operand.data & 0xFF00) >> 8
    state.pc += 3


def inst_JSR(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Absolute16)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Absolute16:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xE2
    state.output_bytes[state.pc+1] = (operand.data & 0x00FF)
    state.output_bytes[state.pc+2] = (operand.data & 0xFF00) >> 8
    state.pc += 3


# Math
def inst_ADC(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x60
        state.output_bytes[state.pc+1] = (operand.data & 0x00FF)
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x61
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3


def inst_SBC(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x70
        state.output_bytes[state.pc+1] = (operand.data & 0x00FF)
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x71
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3


def inst_CMP(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x68
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.Absolute8 or operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x69
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_CPX(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x78
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.Absolute8 or operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x79
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_CPY(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x7C
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.Absolute8 or operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x7B
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_SEC(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x08
    state.pc += 1


def inst_CLC(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x09
    state.pc += 1


def inst_RTS(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x0F
    state.pc += 1


# Branches
def inst_BRA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Relative)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Relative:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xC0
    state.output_bytes[state.pc+1] = operand.data
    state.pc += 2


def inst_BEQ(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Relative)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Relative:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xC3
    state.output_bytes[state.pc+1] = operand.data
    state.pc += 2


def inst_BNE(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Relative)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Relative:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xC4
    state.output_bytes[state.pc+1] = operand.data
    state.pc += 2


def inst_BCC(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Relative)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Relative:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xC1
    state.output_bytes[state.pc+1] = operand.data
    state.pc += 2


def inst_BCS(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Relative)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Relative:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xC2
    state.output_bytes[state.pc+1] = operand.data
    state.pc += 2


def inst_BVC(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Relative)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Relative:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xC5
    state.output_bytes[state.pc+1] = operand.data
    state.pc += 2


def inst_BVS(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Relative)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Relative:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xC6
    state.output_bytes[state.pc + 1] = operand.data
    state.pc += 2


def inst_BMI(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, expected_mode=AddressingMode.Relative)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Relative:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xC7
    state.output_bytes[state.pc + 1] = operand.data
    state.pc += 2


# register-register transfers
def inst_TAX(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x18
    state.pc += 1


def inst_TAY(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x19
    state.pc += 1


def inst_TXA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x1A
    state.pc += 1


def inst_TYA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x1B
    state.pc += 1


def inst_TAS(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x1C
    state.pc += 1


def inst_TSA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x1D
    state.pc += 1


def inst_TAV(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x1E
    state.pc += 1


def inst_TVA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x1F
    state.pc += 1


def inst_SYS(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Immediate8:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x40
    state.output_bytes[state.pc+1] = operand.data
    state.pc += 2


def inst_SYR(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0x0E
    state.pc += 1


def inst_ASL(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Implied:
        state.output_bytes[state.pc] = 0x80
        state.pc += 1
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x81
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        state.output_bytes[state.pc] = 0x82
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_LSR(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Implied:
        state.output_bytes[state.pc] = 0x88
        state.pc += 1
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x89
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        state.output_bytes[state.pc] = 0x8A
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_OR(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x90
        state.output_bytes[state.pc+1] = operand.data
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x91
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        state.output_bytes[state.pc] = 0x92
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_AND(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0x98
        state.output_bytes[state.pc + 1] = operand.data
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0x99
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        state.output_bytes[state.pc] = 0x9A
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_XOR(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Immediate8:
        state.output_bytes[state.pc] = 0xA0
        state.output_bytes[state.pc + 1] = operand.data
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0xA1
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        state.output_bytes[state.pc] = 0xA2
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_ROL(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Implied:
        state.output_bytes[state.pc] = 0xA8
        state.pc += 1
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0xA9
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        state.output_bytes[state.pc] = 0xAA
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_ROR(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.Implied:
        state.output_bytes[state.pc] = 0xB0
        state.pc += 1
    elif operand.addressing_mode == AddressingMode.Absolute16:
        state.output_bytes[state.pc] = 0xB1
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    elif operand.addressing_mode == AddressingMode.Absolute16OffsetX:
        state.output_bytes[state.pc] = 0xB2
        state.output_bytes[state.pc + 1] = (operand.data & 0x00FF)
        state.output_bytes[state.pc + 2] = (operand.data & 0xFF00) >> 8
        state.pc += 3
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def inst_PHA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xD0
    state.pc += 1


def inst_PHX(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xD1
    state.pc += 1


def inst_PHY(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xD2
    state.pc += 1


def inst_PHP(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xD3
    state.pc += 1


def inst_PLA(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xD4
    state.pc += 1


def inst_PLX(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xD5
    state.pc += 1


def inst_PLY(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xD6
    state.pc += 1


def inst_PLP(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state)

    # Determine the addressing mode.
    if operand.addressing_mode != AddressingMode.Implied:
        return Error.ERR_INVALID_ADDRESSING_MODE

    state.output_bytes[state.pc] = 0xD7
    state.pc += 1


KnownInstructions = {
    'INC': inst_INC,
    'INA': inst_INA,
    'INX': inst_INX,
    'INY': inst_INY,

    'DEC': inst_DEC,
    'DEA': inst_DEA,
    'DEX': inst_DEX,
    'DEY': inst_DEY,

    'LDA': inst_LDA,
    'LDX': inst_LDX,
    'LDY': inst_LDY,

    'STA': inst_STA,
    'STX': inst_STX,
    'STY': inst_STY,
    'BRK': inst_BRK,

    'BRA': inst_BRA,

    'SEC': inst_SEC,
    'CLC': inst_CLC,

    'ADC': inst_ADC,
    'SBC': inst_SBC,

    'CMP': inst_CMP,
    'CPX': inst_CPX,
    'CPY': inst_CPY,

    'JMP': inst_JMP,
    'JSR': inst_JSR,
    'RTS': inst_RTS,

    'ASL': inst_ASL,
    'LSR': inst_LSR,

    'OR': inst_OR,
    'AND': inst_AND,
    'XOR': inst_XOR,
    'ROL': inst_ROL,
    'ROR': inst_ROR,

    'NOP': inst_NOP,

    'BEQ': inst_BEQ,
    'BNE': inst_BNE,
    'BCC': inst_BCC,
    'BCS': inst_BCS,
    'BMI': inst_BMI,
    'BVC': inst_BVC,
    'BVS': inst_BVS,

    'TAX': inst_TAX,
    'TAY': inst_TAY,
    'TXA': inst_TXA,
    'TYA': inst_TYA,
    'TAS': inst_TAS,
    'TSA': inst_TSA,
    'TAV': inst_TAV,
    'TVA': inst_TVA,

    'PHA': inst_PHA,
    'PHX': inst_PHX,
    'PHY': inst_PHY,
    'PHP': inst_PHP,
    'PLA': inst_PLA,
    'PLX': inst_PLX,
    'PLY': inst_PLY,
    'PLP': inst_PLP,

    # Synonyms
    'BLT': inst_BCC,
    'BGE': inst_BCS,
}

