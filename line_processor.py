import assembler_types
import directives
import instructions
import statements


def process_directive(state: assembler_types.AssemblyState, line: str):
    # Process an assembly directive.

    # Read the first word of the line and see if it's a type we recognize.
    directive_name = line.split(" ")[0].lstrip(".").upper()
    directive_value = " ".join(line.split(" ")[1:]).upper()
    print("Processing directive: {0}".format(directive_name))

    if directive_name in directives.KnownDirectives:
        return directives.KnownDirectives[directive_name](state, directive_value)

    return "Unknown directive"


def process_label(state: assembler_types.AssemblyState, line: str):
    # Process a label.

    # Does this label already exist?
    label_name = line[0:-1]
    if label_name in state.symbols:
        return "Label already defined"

    # Label does not exist, add it to the list of symbols.
    else:
        state.symbols[label_name.upper()] = state.pc
        state.previous_global_label = label_name.upper()
        return None


def process_local_label(state: assembler_types.AssemblyState, line: str):
    # Process a local label.

    # Does this label already exist?
    label_name = line[0:-1]
    if label_name in state.symbols:
        return "Label already defined"

    # Label does not exist, add it to the list of labels.
    # Local labels need to have the previous GLOBAL label prepended to their name.
    else:
        state.symbols[str(state.previous_global_label) + str(label_name.upper())] = state.pc
        return None


def process_symbols(state: assembler_types.AssemblyState, line:str, lineNum: int):
    # Look for the symbols in the source code.

    line = line.strip()

    if line == "":
        return None # Blank line, don't do anything.

    elif line.startswith(";"):
        return None # Command, don't do anything.

    elif line.startswith("%"):
        error = process_local_label(state, line)

    # Is this a label?
    elif line.endswith(":"):
        error = process_label(state, line)




def process_sourcecode_line(state: assembler_types.AssemblyState, line: str, lineNum: int):
    # Process one line of source code.
    
    # Remove beginning and ending whitespace.
    line = line.strip()

    if line == "":
        return  # Blank line, don't do anything.

    elif line.startswith(";"):
        return  # Command, don't do anything.

    elif line.startswith(".") and line.endswith(":"):
        state.local_labels[line[:-1].upper()] = state.pc
        return

    # Is this a label?
    elif line.endswith(":"):
        state.symbols[line[:-1].upper()] = state.pc
        return

    # Is this a directive?
    elif line.startswith("."):
        error = process_directive(state, line)

    # elif len(line.split(" ")) == 1 or len(line.split(" ")) == 2:
    # Try parsing this as a statement or instruction.
    elif len(line.split(" ")) >= 1:
        value = None

        # Try parsing an instruction.
        instruction = line.split(" ")[0].upper()

        if len(line.split(" ")) >= 2:
            value = " ".join(line.split(" ")[1:])

        if instruction in instructions.KnownInstructions:
            error = instructions.KnownInstructions[instruction](state, value)
        elif instruction in statements.KnownStatements:
            error = statements.KnownStatements[instruction](state, value)
        else:
            error = "Line not recognized"

    # Check if we encountered an error during processing.
    if error is not None:
        print("Error processing line {0} '{1}': {2}".format(lineNum, line, error))
        return True

    return False
