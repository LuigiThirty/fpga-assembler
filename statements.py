# Statements are any recognized line that is not an instruction.

import assembler_types
from assembler_types import AddressingMode, Operand, Error

letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"


def stmt_DB(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, is_statement=True)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.StringLiteral:
        for i in operand.data:
            state.output_bytes[state.pc] = int(ord(i))
            state.pc += 1
    elif operand.addressing_mode == AddressingMode.ByteLiteral:
        state.output_bytes[state.pc] = operand.data
        state.pc += 1
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


def stmt_DW(state: assembler_types.AssemblyState, value: str):
    operand = Operand(value, state, is_statement=True)

    # Determine the addressing mode.
    if operand.addressing_mode == AddressingMode.ByteLiteral:
        state.output_bytes[state.pc] = operand.data
        state.output_bytes[state.pc+1] = 0
        state.pc += 2
    elif operand.addressing_mode == AddressingMode.WordLiteral:
        state.output_bytes[state.pc] = operand.data & 0x00FF
        state.output_bytes[state.pc+1] = (operand.data & 0xFF00) >> 8
        state.pc += 2
    else:
        return Error.ERR_INVALID_ADDRESSING_MODE


KnownStatements = {
    "DB": stmt_DB,
    "DW": stmt_DW,
}
