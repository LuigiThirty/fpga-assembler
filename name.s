    .org $F000

    ; UART equates
    .equ UART_DATA      $A000
    .equ UART_STATUS    $A001

    ; 4-digit LED equates
    .equ LED_HIBYTE     $9000
    .equ LED_LOBYTE     $9001

    ; Video equates
    .equ TILE_ADDRESS_LO    $8000
    .equ TILE_ADDRESS_HI    $8001
    .equ TILE_DATA          $8002

    ; Lower memory variables
    ; Stack is at $0100-$01FF

    .equ BCDBUF_0 $70
    .equ BCDBUF_1 $71
    .equ BCDBUF_2 $72
    .equ BCDBUF_3 $73

    ; Current text cursor location.
    .equ CURSOR_X $80
    .equ CURSOR_Y $81

    .equ INPUT_LENGTH   $82  ; How many characters are in the input buffer?
    .equ INPUT_BUFFER   $0200  ; Location of the actual input buffer.

Entry:
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP

    ; Set up stack
    LDA #$FF
    TAS

    LDA #$A5
    STA $0005

    ; Initialize variables
    LDA #0
    STA CURSOR_X
    STA CURSOR_Y

    STA INPUT_LENGTH

    LDA #<STR_Name
    STA $0000
    LDA #>STR_Name
    STA $0001
    JSR PrintString

    BRK

STR_Name:
    .asciiz "WHAT IS YOUR NAME?"

;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;
MoveCursor:
    ; X:Y -> 16-bit tile address, little endian
    STX $8000
    STY $8001
    RTS

MoveCursorDebug:
    ; X:Y -> 16-bit tile address, little endian

    ; One row is 40 tiles wide.
    ; To convert (col,row) to address:
    ;   address = (row * 40) + col

    ; But we're using a lookup table.
    ; $02 = lo $03 = hi
    TYA
    ASL
    TAY

    LDA TileYLookup,Y
    STA $0002
    INY
    LDA TileYLookup,Y
    STA $0003

    TXA
    BCC MoveCursorDebug_Write

    INC $0003

MoveCursorDebug_Write:
    ;STA $0002

    LDA $0002
    STA $8000
    LDA $0003
    STA $8001

    RTS

PrintString:
    LDY #0

    LDA $0000
    STA $9001
    LDA $0001
    STA $9000

PrintString_Loop:
    ; The string pointer is at $00.
    LDA ($0000),Y
    CMP #0
    BEQ PrintString_Done

    STA $8002
    INY
    BRA PrintString_Loop

PrintString_Done:
    RTS