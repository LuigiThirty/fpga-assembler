    .org $F000

    ; UART equates
    .equ UART_DATA      $A000
    .equ UART_STATUS    $A001

    ; 4-digit LED equates
    .equ LED_HIBYTE     $9000
    .equ LED_LOBYTE     $9001

    ; Video equates
    .equ TILE_ADDRESS_LO    $8000
    .equ TILE_ADDRESS_HI    $8001
    .equ TILE_DATA          $8002

    ; Lower memory variables
    ; Stack is at $0100-$01FF

    .equ HEXBUF_0 $70
    .equ HEXBUF_1 $71
    .equ HEXBUF_2 $72
    .equ HEXBUF_3 $73

    ; Current text cursor location.
    .equ CURSOR_X $80
    .equ CURSOR_Y $81

    .equ INPUT_LENGTH   $82  ; How many characters are in the input buffer?
    .equ INPUT_BUFFER   $0200  ; Location of the actual input buffer.

Entry:
    ; TODO: hold CPU in reset for one second on power-up
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP
    NOP

    ; Set up stack
    LDA #$FF
    TAS

    LDA #$A5
    STA $0005

    ; Initialize variables
    LDA #0
    STA CURSOR_X
    STA CURSOR_Y

    STA INPUT_LENGTH

CommandInputStart:
    LDA #0
    STA INPUT_LENGTH
    JSR PrintCommandPrompt

InputLoop:
    JSR WaitForSerialByte

    ; Grab the character from the FIFO
    LDA UART_DATA
    JMP ProcessInputChar

    JMP InputLoop

    BRK

    ; TODO: local labels

;;;;;;;;;;;;;;;;;;;;
ProcessInputChar:
    ; Append the character to the input buffer.
    LDX INPUT_LENGTH
    STA $0200,X
    INC INPUT_LENGTH

    ; Is it a CR?
    CMP #$0D
    BEQ ProcessCR

    ; Try printing any other character we get.
    JSR PutChar
%Done:
    JMP InputLoop

;;;;;;;;;;;;;;
    ; Process a carriage return: Marks the end of an input line.
ProcessCR:
    JSR HWCursor_CR

    ; Try converting input to a hex number...
    LDA INPUT_BUFFER        ; start with byte 0 for now
    JSR ConvertToHexNumber  ; Get the hex result
    STA HEXBUF_0            ; Store...

    LDX #1
    LDA INPUT_BUFFER,X      ; start with byte 0 for now
    JSR ConvertToHexNumber  ; Get the hex result
    STA HEXBUF_0,X            ; Store...

    LDX #2
    LDA INPUT_BUFFER,X        ; start with byte 0 for now
    JSR ConvertToHexNumber  ; Get the hex result
    STA HEXBUF_0,X            ; Store...

    LDX #3
    LDA INPUT_BUFFER,X        ; start with byte 0 for now
    JSR ConvertToHexNumber  ; Get the hex result
    STA HEXBUF_0,X            ; Store...


%Done:
    ; OK, now write digits 0 and 1 to the display for debugging
    LDA HEXBUF_0
    STA $9000
    LDA HEXBUF_1
    STA $9001

    JMP CommandInputStart

;;;;;;;;;;;;;;;;;;;;
ConvertToHexNumber:
    ; Checks if A contains an ASCII value 0-9 or A-F.
    ; If it does, returns A=hex value. Else, returns A=$FF.

    SEC
    SBC #$30 ; 0-9 is now 0-9; A-F is now $11-$15
    CMP #$A
    BLT %IsHex

    ; It's not 0-9, maybe it's A-F.
    SEC
    SBC #7      ; subtract 7 to turn $11 into $A.
    CMP #$10    ; is it below $10?
    BGE %IsNotHex

%IsHex:
    RTS

%IsNotHex:
    LDA #$FF
    RTS


;;;;;;;;;;;;;;;;;;;;
WaitForSerialByte:
    ; If the FIFO is empty, $A000.7 will be set.
    ; If the FIFO is full,  $A000.6 will be set.
    LDA $A001
    CMP #$80

    BEQ WaitForSerialByte

    RTS ; We have a byte, return

;;;;;;;;;;;;;;;;

PrintCommandPrompt:
    LDA #'>'
    JSR PutChar
    RTS

;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;
PutChar:
    ; Outputs the byte in A to the tile data port.
    ; Advances the cursor by one position, wrapping if necessary.
    STA $8002
    JSR AdvanceCursor
    RTS

AdvanceCursor:
    ; Advances the cursor X by one position.
    ; If cursor X has hit the right bound, wraps to X = 0 and increments Y by 1.
    PHA
    INC CURSOR_X
    LDA #40
    CMP CURSOR_X
    BNE %Done

%Wrap:
    INC CURSOR_Y
    LDA #0
    STA CURSOR_X

%Done:
    PLA
    RTS


MoveHWCursor:
    ; Update hardware cursor to location in memory.

    ; One row is 40 tiles wide.
    ; To convert (col,row) to address:
    ;   address = (row * 40) + col

    ; But we're using a lookup table.
    ; $02 = lo $03 = hi

    LDA CURSOR_Y
    ASL
    TAY

    LDA TileYLookup,Y
    STA $0002
    INY
    LDA TileYLookup,Y
    STA $0003

    LDA CURSOR_X
    CLC
    ADC $0002
    BCC %Write

    INC $0003

%Write:
    LDA $0002
    STA $8000
    LDA $0003
    STA $8001

    RTS

HWCursor_CR:
    ; Perform a carriage return. Sets X to 0 and increments Y.
    LDA #0
    STA CURSOR_X
    INC CURSOR_Y

    JSR MoveHWCursor

    RTS

    ; Lookup table that converts tile Y to memory offset.
TileYLookup:
    dw 0
    dw 40
    dw 80
    dw 120
    dw 160
    dw 200
    dw 240
    dw 280
    dw 320
    dw 360
    dw 400
    dw 440
    dw 480
    dw 520
    dw 560
    dw 600
    dw 640
    dw 680
    dw 720
    dw 760
    dw 800
    dw 840
    dw 880
    dw 920
    dw 960

;;;;;;;;;;;
DEBUG_PutCursorPosOnDisplay:
    LDA CURSOR_X
    STA $9000
    LDA CURSOR_Y
    STA $9001

    RTS