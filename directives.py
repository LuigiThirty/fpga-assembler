import assembler_types
import re

def directive_org(state: assembler_types.AssemblyState, value):
    if state.pc == 0:
        state.initial_pc = int(value.replace("$", "0x"), 16)
    state.pc = int(value.replace("$", "0x"), 16)
    return None


def directive_asciiz(state: assembler_types.AssemblyState, value):
    if value[0] == "\"" and value[-1] == "\"":
        for c in value[1:-1]:
            state.output_bytes[state.pc] = ord(c)
            state.pc += 1
        state.output_bytes[state.pc] = 0
        state.pc += 1
    else:
        return "Invalid ASCII string"


def directive_equ(state: assembler_types.AssemblyState, value):

    rex = re.compile(r'\s+')
    result = rex.sub(" ", value)
    tokens = result.split(" ")

    state.symbols[tokens[0]] = assembler_types.evaluate_expression(" ".join(tokens[1:]))
    return None


KnownDirectives = {'ORG': directive_org,
                   'ASCIIZ': directive_asciiz,
                   "EQU": directive_equ}
