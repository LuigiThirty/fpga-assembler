import line_processor
import assembler_types

state = assembler_types.AssemblyState()


def assemble_file(filename):
    error_count = 0

    print("Assembling file: {0}".format(filename))

    file = open(filename, "r")
    sourcecode = file.read()

    lineNum = 0

    print("Symbol pass")
    for line in sourcecode.splitlines():
        lineNum += 1
        error = line_processor.process_symbols(state, line, lineNum)
        if error:
            error_count += 1

    lineNum = 0

    print("Assembly pass")
    for line in sourcecode.splitlines():
        lineNum += 1

        # Is there a semicolon anywhere in the line? If so, discard it and everything after it.
        if line.find(";") != -1:
            line = line[0:line.find(";")]

        error = line_processor.process_sourcecode_line(state, line, lineNum)
        if error:
            error_count += 1

    if len(state.replace_with_symbol_locations) > 0:
        print("Symbol replacement pass")

        for k, v in state.replace_with_symbol_locations.items():
            up = ""

            # TODO: clean this up so all symbols are dicts
            if type(v.symbol) is dict:
                up = v.symbol["result"]
            else:
                up = v.symbol.upper()

            # If this is an instruction with a comma that does not end in X or Y, it's illegal.

            if "," in up:
                # Get everything before the first ,
                up = up[:up.find(",")]

            if up in state.symbols:
                # print("Symbol '{0}' == {1}".format(v.symbol, state.symbols[up]))
                if v.addressing_mode == assembler_types.AddressingMode.Relative:
                    branch_size = state.symbols[up] - k - 1
                    if abs(branch_size) > 127:
                        print("Branch out of range")
                        error_count += 1
                    else:
                        if branch_size > 0:
                            state.output_bytes[k] = branch_size  # Account for the one-byte instruction.
                        else:
                            state.output_bytes[k] = abs(branch_size) | 0x80 # Signed value
                elif v.addressing_mode == assembler_types.AddressingMode.Absolute16:

                    if type(v.symbol) is dict:
                        state.output_bytes[k] = state.symbols[up]["result"] & 0x00FF
                        state.output_bytes[k + 1] = (state.symbols[up]["result"] & 0xFF00) >> 8
                    else:
                        state.output_bytes[k] = state.symbols[up] & 0x00FF
                        state.output_bytes[k+1] = (state.symbols[up] & 0xFF00) >> 8
                elif v.addressing_mode == assembler_types.AddressingMode.Immediate8:
                    # Is this one byte of a word-sized symbol?
                    if type(v.symbol) is dict:
                        if v.symbol["only_hibyte"]:
                            state.output_bytes[k] = (state.symbols[up] & 0xFF00) >> 8
                        elif v.symbol["only_lobyte"]:
                            state.output_bytes[k] = (state.symbols[up] & 0xFF)
                else:
                    raise ValueError("Internal assembler error: could not find addressing mode for symbol replacement")

            else:
                print("Undefined symbol: {0}".format(v.symbol))
                error_count += 1

    return error_count


errors = assemble_file("monitor.s")
print("\nErrors: {0}".format(errors))

if errors == 0:
    print("Writing output file...")
    out_v = open("program.vmem", "w")
    out = open("program.bin", "wb")

    # Write in Verilog memory format, not raw binary.
    for b in range(state.initial_pc, state.pc):
        out_v.write(str(hex(state.output_bytes[b]))[2:])
        out_v.write("\n")

    # Write in raw binary.
    out.write(state.output_bytes[state.initial_pc:state.pc])